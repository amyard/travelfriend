import sys
sys.path.append("..")

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.urls import reverse
from django.test import TestCase, Client

from api.tests.factories_users import UserFactory


User = get_user_model()


class CreateUpdateDeleteUserTestCase(TestCase):

    def setUp(self):
        self.user = User.objects._create_user(email = 'asd@gmail.com', username='asd', password='12121212')
        self.user.set_password('121212')
        self.user.save()


    def test_create_user_valid_active(self):
        self.user.is_active = True
        self.user.save()
        self.assertTrue(self.user.is_active)


    def test_new_user_invalid_username(self):
        ''' creating user with no email raises error '''
        with self.assertRaises(ValueError):
            get_user_model().objects._create_user(None, '12121212')

    def test_create_new_superuser(self):
        user = get_user_model().objects.create_superuser('test', 'zaza1234')
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)