import sys
sys.path.append("..")

from django.test import TestCase, Client

from api.tests.factories_locations import PostModelFactory
from api.post.models import Post




class RouteModelFactoryTestCase(TestCase):

    def test_create_route_full_info(self):
        rt = PostModelFactory()
        rt.refresh_from_db()
        cnt = Post.objects.count()
        self.assertEquals(cnt, 1)

    def test_create_default_integer_fields(self):
        rt = Post.objects.create(
            title = 'test 1', 
            content = 'some info here'
        )
        rt.refresh_from_db()
        res = Post.objects.first()
        self.assertEquals(res.group_amount, 10)
        self.assertEquals(res.free_space, 10)

    def test_create_without_content_field(self):
        rt = Post.objects.create(
            title = 'test 1'
        )
        rt.refresh_from_db()
        res = RouteModel.objects.first()
        self.assertEquals(RouteModel.objects.count(), 1)
        self.assertEquals(res.title, 'test 1')

    def test_create_str_result(self):
        rt = RouteModel.objects.create(
            title = 'test 1', 
            content = 'some info here'
        )

        self.assertEquals(rt.__str__(), 'test 1')
