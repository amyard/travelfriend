import uuid
import factory
from faker import Faker
from factory import fuzzy
import random

from api.tests.factories_common import BaseModelFactory
from api.post.models import Post



fake = Faker()


class PostModelFactory(BaseModelFactory):

    title = factory.Sequence(lambda n: 'Route # {}'.format(n)) 
    content = fake.text()
    group_amount = factory.fuzzy.FuzzyDecimal(1, 20)
    free_space = factory.fuzzy.FuzzyDecimal(1, 20)

    class Meta:
        model = Post
