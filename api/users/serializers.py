from rest_framework import serializers
from api.users.models import ProfileUser, User


class UsersSerializer(serializers.ModelSerializer):
    ''' сериализация пользователя '''
    class Meta:
        model = User
        fields = ['id', 'username', 'email']


class ProfileUserSerializer(serializers.ModelSerializer):
    ''' сериализация профиля пользователя '''
    user = UsersSerializer()

    class Meta:
        model = ProfileUser
        fields = ['user', 'avatar']