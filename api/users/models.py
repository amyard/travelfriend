from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import PermissionsMixin


USERNAME_REGEX = '^[a-zA-Z0-9.+-]*$'


class UserManager(BaseUserManager):

    def _create_user(self, username, password, **extra_fields):
        if not username:
            raise ValueError(_('The username must be set'))
        
        # user = self.model(username = username, email = self.normalize_email(email), **extra_fields)
        user = self.model(username = username, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_admin', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        if extra_fields.get('is_admin') is not True:
            raise ValueError(_('Superuser must have is_admin=True.'))
        return self._create_user(username, password, **extra_fields)



class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'), max_length=75, unique=True,
                            validators=[RegexValidator(regex=USERNAME_REGEX,
                                                        message=_('Username must be alphanumeric or contain numbers'),
                                                        code='invalid_username')]
                            )
    email = models.EmailField(_('email'), max_length=255, unique=True)

    is_admin = models.BooleanField(_('is_admin'), default=False)
    is_staff = models.BooleanField(_('is_staff'), default=False)
    is_active = models.BooleanField(_('is_active'), default=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        ordering = ('-id',)

    def __str__(self):
        return self.email

    def get_short_name(self):
        return self.email


    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True


def save_image_path(instance, filename):
    filename = instance.avatar
    return 'avatars/{}/{}'.format(instance.user, filename)


class ProfileUser(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE, related_name='profileuser')
    avatar = models.ImageField(_('avatar'), upload_to=save_image_path, blank = True)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')
        ordering = ['-id']