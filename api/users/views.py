from rest_framework import generics, permissions
from django.shortcuts import get_object_or_404

from api.users.models import ProfileUser
from api.users.serializers import ProfileUserSerializer


class ProfileUserView(generics.RetrieveAPIView):
    """ вывод данный пользователя - меня """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProfileUserSerializer
    queryset = ProfileUser.objects.all()

    def get_object(self):
        obj = get_object_or_404(self.queryset, user = self.request.user)
        self.check_object_permissions(self.request, obj)
        return obj


class ProfileUserPublicView(generics.RetrieveAPIView):
    """ вывод данныx профиля пользователя - других пользователей"""
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProfileUserSerializer
    queryset = ProfileUser.objects.all()