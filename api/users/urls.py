from django.urls import path
from api.users import views

urlpatterns = [
    path('', views.ProfileUserView.as_view()),
    path('<int:pk>/', views.ProfileUserPublicView.as_view()),
]