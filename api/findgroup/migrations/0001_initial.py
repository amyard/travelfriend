# Generated by Django 2.2.7 on 2019-11-28 23:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0002_profileuser'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dashboard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('group_amount', models.PositiveIntegerField(default=10)),
                ('free_space', models.IntegerField(default=10)),
                ('members', models.ManyToManyField(related_name='members', to='users.ProfileUser')),
            ],
        ),
    ]
