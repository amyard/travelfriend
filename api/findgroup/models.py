from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.users.models import ProfileUser
from api.post.models import Tag


# TODO - проверка free_space не может быть больше group_amount вначале
class Dashboard(models.Model):
    title = models.CharField(_('title'), max_length = 255)
    content = models.TextField(_('content'), null=True, blank=True)
    tags = models.ManyToManyField(Tag, related_name='tags', null=True, blank=True)

    owner = models.ForeignKey(ProfileUser, on_delete=models.CASCADE)
    members = models.ManyToManyField(ProfileUser, related_name='members', blank = True, null = True)

    group_amount = models.PositiveIntegerField(_('group_amount'), default=10)
    free_space = models.IntegerField(_('group_amount'), default=10)

    def __str__(self):
        return self.title