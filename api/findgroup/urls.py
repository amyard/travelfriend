from django.urls import path
from . import views

urlpatterns = [
    path('add-member/<int:pk>', views.AddMemberView.as_view()),
    path('create-group/', views.DashboardCreateView.as_view()),
]

