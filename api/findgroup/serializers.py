from rest_framework import serializers

from .models import Dashboard
from api.users.models import ProfileUser


# рассылка писен на почту если подписаны на какие-то тегы
class DashboardCreateSerializer(serializers.ModelSerializer):
    '''  сериализация создания груп  '''
    class Meta:
        model = Dashboard
        fields = ['title', 'content', 'tags', 'group_amount', 'free_space']


class DashboardAddMemberSerializer(serializers.ModelSerializer):
    '''  добавить нового члена в группу  '''
    class Meta:
        model = Dashboard
        fields = ['members', ]