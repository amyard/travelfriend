from django.apps import AppConfig


class FindgroupConfig(AppConfig):
    name = 'api.findgroup'
