from rest_framework import permissions, generics, status
from rest_framework.response import Response

from .serializers import DashboardAddMemberSerializer, DashboardCreateSerializer
from .models import Dashboard

from api.users.models import ProfileUser


class DashboardCreateView(generics.CreateAPIView):
    '''  создать поиск по группе  '''
    serializer_class = DashboardCreateSerializer
    queryset = Dashboard.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(owner = ProfileUser.objects.get(user = self.request.user))



# TODO - проверка, есть ли еще свободные места
class AddMemberView(generics.GenericAPIView):
    ''' вступить юзером в группу на доске '''
    permission_classes = [permissions.IsAuthenticated]
    queryset = Dashboard.objects.filter(free_space__gte = 1)  # если есть свободные места
    serializer_class = DashboardAddMemberSerializer

    def post(self, request, **kwargs):
        group = self.get_object()
        profile = ProfileUser.objects.get(user = self.request.user)
        if not profile in group.members.all():
            group.members.add(profile)
            group.free_space = group.free_space - 1
            group.save()
        return Response(status=status.HTTP_201_CREATED)


    '''  выйти из группы  '''
    def delete(self, request, **kwargs):
        group = self.get_object()
        profile = ProfileUser.objects.get(user=self.request.user)
        if not profile in group.members.all():
            group.members.remove(profile)
            group.free_space = group.free_space + 1
            group.save()
        return Response(status=status.HTTP_204_NO_CONTENT)