from rest_framework import generics, permissions, mixins, status, viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django_filters import rest_framework as rest_filters, NumberFilter, CharFilter
from rest_framework import filters


from .serializers import PostSerializer, PostCreateSerializer, PostCommentCreateSerializer
from .models import Post, PostComment
from api.utils.permissions import OwnerOrAdmin, MixedPermission, CommentOwnerOrAdmin
from api.utils.mixins import CreateUpdateDeleteMixin
from api.users.models import ProfileUser



class PostPagination(PageNumberPagination):
    """Количество записей для пагинации"""
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000



class PostFilter(rest_filters.FilterSet):
    """Фильтр статей"""
    category = CharFilter(field_name='category__name', lookup_expr='icontains')
    tag = CharFilter(field_name='tag__name', lookup_expr='icontains')
    year = NumberFilter(field_name='created', lookup_expr='year')
    month = NumberFilter(field_name='created', lookup_expr='month')

    class Meta:
        model = Post
        fields = ['tag', 'created']


class PostList(generics.ListAPIView):
    """ список всех статтьей """
    permission_classes = [permissions.AllowAny]
    serializer_class = PostSerializer
    pagination_class = PostPagination
    queryset = Post.objects.all()


    # фильтр
    filter_backends = (rest_filters.DjangoFilterBackend, filters.SearchFilter)
    filterset_class = PostFilter
    # поиск   http://127.0.0.1:8000/api/v1/post/?search=Swim
    search_fields = ['title', 'tag__name']


class PostCreateView(generics.CreateAPIView):
    '''  создание статтьи '''
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all()

    # def perform_create(self, serializer):
    #     serializer.save(owner = ProfileUser.objects.get(user = self.request.user))

class PostDeleteView(generics.DestroyAPIView):
    '''  удаление статтьи  - status 204   '''
    permission_classes = [permissions.IsAuthenticated, OwnerOrAdmin]
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all()

class PostUpdateView(generics.UpdateAPIView):
    '''  редактировать данные  '''
    permission_classes = [permissions.IsAuthenticated, OwnerOrAdmin]
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all()

class PostDetailView(generics.RetrieveAPIView):
    '''  детализация поста  '''
    permission_classes = [permissions.AllowAny]
    serializer_class = PostSerializer
    queryset = Post.objects.all()



# class PostCommentViewApi(CreateUpdateDeleteMixin):
#     '''  комментарии для постов  '''
#     permission_classes = [OwnerOrAdmin]
#     queryset = PostComment.objects.all()
#     serializer_class = PostCommentCreateSerializer
#     permission_classes_by_action = {'create':[OwnerOrAdmin],
#                                     'update':[OwnerOrAdmin],
#                                     'destroy':[OwnerOrAdmin]}
#
    # def perform_create(self, serializer):
    #     serializer.save(owner = ProfileUser.objects.get(user = self.request.user))


class PostCommentViewApi(CreateUpdateDeleteMixin):
    '''  CRUD комментарии для постов  '''
    permission_classes = [CommentOwnerOrAdmin]
    queryset = PostComment.objects.filter(deleted = False)
    serializer_class = PostCommentCreateSerializer


    def perform_create(self, serializer):
        serializer.save(owner = ProfileUser.objects.get(user = self.request.user))

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.save()














# class CommentListViewApi(generics.ListAPIView):
#     '''  список комментариев  '''
#     serializer_class = CommentListSerializer
#     queryset = Comment.objects.all()
#     permission_classes = [permissions.AllowAny]
#
#
#
# class CommentViewApi(mixins.CreateModelMixin,
#                      mixins.RetrieveModelMixin,
#                      mixins.UpdateModelMixin,
#                      mixins.DestroyModelMixin,
#                      generics.GenericAPIView):
#     permission_classes = [permissions.IsAuthenticated]
#     queryset = Comment.objects.all()
#     serializer_class = CommentSerializer
#
#     '''  создание комментов в группе  '''
#     def post(self, request, *args, **kwargs):
#         # проверка - существует ли такая статтья и если parent comment
#         parent = 1 if self.request.data['parent'] == '' else self.request.data['parent']
#         if Comment.objects.filter(
#                 post__id = self.request.data['post'],
#                 parent = parent
#         ).exists:
#             return super().create(request, *args, **kwargs)
#         else:
#             return Response(status = status.HTTP_404_NOT_FOUND)
#
#     def perform_create(self, serializer):
#         serializer.save(owner = ProfileUser.objects.get(user = self.request.user))
#
#     ''' удаление комментов в группе  '''
#     def delete(self, request, *args, **kwargs):
#         instance = self.get_object()
#         if self.request.user.is_superuser or self.request.user == instance.owner:
#             return super().destroy(request, *args, **kwargs)  # return status 204
#         else:
#             return Response(status = status.HTTP_404_NOT_FOUND)
#
#
#     def put(self, request, *args, **kwargs):
#         if self.check(request, *args, **kwargs):
#             return super().update(request, *args, **kwargs)
#         else:
#             return Response(status=status.HTTP_404_NOT_FOUND)
#
#     ''' detail '''
#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)