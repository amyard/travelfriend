# Generated by Django 2.2.7 on 2019-12-04 23:47

from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0010_auto_20191203_2225'),
    ]

    operations = [
        migrations.AddField(
            model_name='postcomment',
            name='deleted',
            field=models.BooleanField(default=False, verbose_name='was deleted?'),
        ),
        migrations.AlterField(
            model_name='postcomment',
            name='parent',
            field=mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='children', to='post.PostComment', verbose_name='parent comment'),
        ),
    ]
