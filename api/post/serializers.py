from rest_framework import serializers
from rest_framework.serializers import PrimaryKeyRelatedField
from rest_framework_recursive.fields import RecursiveField

from api.users.serializers import ProfileUserSerializer
from .models import Tag, PostComment, Post
from api.users.models import ProfileUser


# два сериалайзера дабы отображать комменты как положено, с рекурсиев и двером,
# а не дверо + дублирование потом всем комментов опять
class FilterCommentListSerializer(serializers.ListSerializer):
    '''   фильтр комментариев, только parents  '''
    def to_representation(self, data):
        data = data.filter(parent = None)
        return super().to_representation(data)

class RecursiveCommentSerializer(serializers.Serializer):
    '''  вывод children в комментарии  '''
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context = self.context)
        return serializer.data


class PostCommentListSerializer(serializers.ModelSerializer):
    '''  список комментариев  '''
    owner = serializers.ReadOnlyField(source='author.user.username')
    children = RecursiveCommentSerializer(many = True)
    text = serializers.SerializerMethodField()

    # get text data
    def get_text(self, obj):
        if obj.deleted:
            return None
        return obj.text


    class Meta:
        list_serializer_class = FilterCommentListSerializer
        model = PostComment
        fields = ['id', 'post', 'owner','created', 'updated','text', 'children']

class PostCommentCreateSerializer(serializers.ModelSerializer):
    '''  редактирование комментариев  '''
    children = serializers.ListField(source='get_children', read_only=True, child=RecursiveField())

    class Meta:
        model = PostComment
        fields = ['post', 'text', 'children']




class TagSerializer(serializers.ModelSerializer):
    """ сериализация тэгов """
    class Meta:
        model = Tag
        fields = ['id', 'name']







class PostSerializer(serializers.ModelSerializer):
    """ сериализация списка статтье - listview """
    author = serializers.ReadOnlyField(source='author.user.username')
    tag = TagSerializer(many=True)
    comment = PostCommentListSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'author', 'title', 'content', 'image', 'created', 'modified', 'tag', 'comment']


class PostCreateSerializer(serializers.ModelSerializer):
    """ сериализация создания статтье """

    # tag = TagSerializer(many=True)

    class Meta:
        model = Post
        fields = ['title', 'tag', 'image', 'content', 'group_amount', 'free_space']

    def create(self, validated_data):
        profile = ProfileUser.objects.get(user = self.context['request'].user)

        obj = Post(
            author = profile,
            title = validated_data['title'],
            image = validated_data['image'],
            group_amount = validated_data['group_amount'],
            free_space = validated_data['free_space'],
            content = validated_data['content'],
        )
        obj.save()
        obj.tag.set(validated_data['tag'])
        return obj
