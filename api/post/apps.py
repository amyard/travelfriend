from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class LocationsConfig(AppConfig):
    name = 'api.post'
    verbose_name = _('post')

    def ready(self):
        import api.post.signals