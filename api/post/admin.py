from django.contrib import admin
from api.post.models import Post, Tag, PostComment




@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Post)
class PostAdminModal(admin.ModelAdmin):
    list_display = ('title', 'slug', 'created', 'modified')
    list_filter = ("created", 'modified')
    search_fields = ("title", "tag")
    prepopulated_fields = {"slug": ("title",)}


@admin.register(PostComment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['post', 'owner', 'created', 'updated', 'id']
