from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from mptt.models import MPTTModel, TreeForeignKey

from api.users.models import ProfileUser
from api.utils.models import AbstractComment
from api.utils.transliteration import transliteration_rus_eng



class Tag(models.Model):
    """Класс модели тегов"""
    name = models.CharField(_('name'), max_length=50, null=True)
    slug = models.SlugField(_('slug'), max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = _('tags')
        verbose_name_plural = _('tags')

    def __str__(self):
        return self.name




class Post(TimeStampedModel):
    author = models.ForeignKey(ProfileUser, on_delete=models.CASCADE, verbose_name='author')

    title = models.CharField(_('title'), max_length = 255)
    slug = models.SlugField(_('slug'), max_length=500, blank=True, null=True, unique=True)
    content = models.TextField(_('content'), blank = True, null = True)
    image = models.ImageField(_('image'), upload_to="post/", blank=True, null=True)
    tag = models.ManyToManyField(Tag, verbose_name="Tag", blank=True, related_name='tag')

    group_amount = models.PositiveIntegerField(_('group_amount'), default = 10)
    free_space = models.IntegerField(_('free_space'), default = 10)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = transliteration_rus_eng(self.title)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        ordering = ['-id']


class PostComment(AbstractComment, MPTTModel):
    '''  наследую от абстрактной модели  '''
    owner = models.ForeignKey(ProfileUser, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comment')
    parent = TreeForeignKey(
        "self",
        verbose_name=_("parent comment"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='children')

    def __str__(self):
        return 'Comment for post {}'.format(self.post)

    class Meta:
        verbose_name = _('post comment')
        verbose_name_plural = _('post comments')