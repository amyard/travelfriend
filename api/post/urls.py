from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register('', views.PostCommentViewApi)



urlpatterns = [
    path('', views.PostList.as_view()),
    path('create/', views.PostCreateView.as_view()),
    path('delete/<int:pk>', views.PostDeleteView.as_view()),
    path('update/<int:pk>', views.PostUpdateView.as_view()),
    path('detail/<int:pk>', views.PostDetailView.as_view()),

    path('comments/', include(router.urls)),


    # path('comment/create/', views.CommentViewApi.as_view()),
    # path('comment/delete/<int:pk>/', views.CommentViewApi.as_view()),
]