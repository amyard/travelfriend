from rest_framework import mixins, viewsets

from api.utils.permissions import MixedPermission


class CreateUpdateDeleteMixin(mixins.CreateModelMixin,
                              mixins.UpdateModelMixin,
                              mixins.DestroyModelMixin,
                              MixedPermission,
                              viewsets.GenericViewSet):
    pass