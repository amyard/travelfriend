from django.db import models
from django.utils.translation import ugettext_lazy as _


class AbstractComment(models.Model):
    '''   абстрактная модель  '''
    text = models.TextField()
    created = models.DateTimeField(_('created'), auto_now_add=True)
    updated = models.DateTimeField(_('modified'), auto_now=True)
    deleted = models.BooleanField(_('was deleted?'), default = False)

    class Meta:
        verbose_name = _('comment')
        verbose_name_plural = _('comments')
        abstract  = True

    def __str__(self):
        return self.text