from rest_framework import permissions


class MixedPermission:
    """Миксин permissions для action"""
    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

class OwnerOrAdmin(permissions.BasePermission):
    message = 'Only owner or admin can change data'

    def has_object_permission(self, request, view, obj):
        return request.user.is_admin or request.user == obj.author.user


class CommentOwnerOrAdmin(permissions.BasePermission):
    message = 'Only owner or admin can change comment'

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return request.user.is_admin or request.user == obj.author.user
